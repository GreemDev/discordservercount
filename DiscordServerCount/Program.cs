﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace DiscordServerCount
{
    internal class Program
    {
        private static DiscordSocketClient _client = new DiscordSocketClient();
        
        public static void Main()
        {
            Console.Title = "Discord Server Count";
            Console.CursorVisible = false;
            Console.ForegroundColor = ConsoleColor.Green;
            new Program().LoginAsync().GetAwaiter().GetResult();
        }

        private async Task LoginAsync()
        {
            if (!File.Exists("token.txt"))
            {
                Console.WriteLine("Your token.txt doesn't exist. Creating it now.");
                File.Create("token.txt");
                Console.WriteLine("Close this window to stop the program.");
                await Task.Delay(-1);
                return;
            }
            var token = File.ReadAllText("token.txt");
            await _client.LoginAsync(TokenType.User, token);
            await _client.StartAsync();
            _client.Ready += OnClientReady;
            _client.Log += Log;
            await Task.Delay(-1);


        }

        public static async Task OnClientReady()
        {
            Console.Clear();
            Console.WriteLine($"You're in {_client.Guilds.Count} servers.");
            Console.WriteLine("Close this window to stop the program.");
        }

        public async Task Log(LogMessage s)
        {
            if (s.Message == "Connecting" || s.Message == "Connected") Console.WriteLine(s.Message);
        }
    }
}